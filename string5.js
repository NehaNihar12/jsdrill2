//Convert an array to string.If the array is empty, return an empty string.
function convertArrayToString(arr){
    //input test: 1. isArray 2. Array not empty 3. array entries are strings
    let valid=true
    if(arr.length===0 || !Array.isArray(arr)){
        return ''
    }
    for(let i=0;i<arr.length;i++){
        if(typeof arr[i]!=='string'){
            valid=false
        }
    }
    

    if(valid){
        return arr.join(' ').trim()+'.'
    }else{
        return 'invalid input'
    }

}
module.exports = convertArrayToString