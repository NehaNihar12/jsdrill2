//Function to return the full name in title case.
/* input format
1. values of firstname,middlename,lastname are only words.
*/
function TitleCaseName(nameObj){

    let name1=''
    for(let name in nameObj){
        let halfName = nameObj[name]
        name1 = name1+halfName+' ';
    }
    //formatting names in title case
    let namearr = name1.trim().toLowerCase().split(' ')
     
    let fullName=''
    for(let str of namearr){

        fullName = fullName+(str[0].toUpperCase()+str.slice(1))+' '
    }
    return fullName.trim() 
}

module.exports = TitleCaseName